Proyecto de Grado para la correcta Gestion de Tesis en la UJAP

#LibraryApp

## Instalación

+ Clonar proyecto de este repositorio asi:
		
		$ git clone https://gitlab.com/david17art/libraryApp.git


+ Después de descargar el proyecto entramos a este.

        $ cd nombreRepositorio

+ Ejecutamos el siguiente comando.

        $ composer install
+ Ejecutamos el siguiente comando.

        $ npm install
    
+ Modificamos el nombre del archivo __.env.example.__ por __.env__ y agregamos nuestras credenciales.


+ Por ultimo solo debemos generar una key para nuestra app.

         $ php artisan key:generate

+ Listo ya podemos ejecutar el proyecto Cinema.

        $ php artisan serve