<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProyectMember extends Model
{
	protected $table = "proyect_members";

	protected $fillable = ['user_id'];

    public function proyect(){
        return $this->belongsTo(Proyect::class);
    }   
    public function user(){
        return $this->belongsTo(User::class);
    }    
}
