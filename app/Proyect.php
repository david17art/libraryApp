<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proyect extends Model
{
    protected $table = 'proyects';
    protected $fillable = ['title','description','type','file','aprobe','linea','grado','programa','esp','fase','file2'];

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function member(){
        return $this->hasMany(ProyectMember::class);
    }
    public function tutor(){
        return $this->hasOne(ProyectTutor::class);
    }    
    public function jury(){
        return $this->hasMany(ProyectJury::class);
    }
}
