<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProyectTutor extends Model
{
	protected $table = "proyect_tutors";

	protected $fillable = ['user_id'];

    public function proyect(){
        return $this->hasOne(Proyect::class);
    }   
    public function user(){
        return $this->belongsTo(User::class);
    }   
}
