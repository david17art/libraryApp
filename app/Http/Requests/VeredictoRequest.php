<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VeredictoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'proyecto' => 'required',
            'presidente' => 'required|string',
            'cedulaP' => 'required|integer',
            'miembro1' => 'required|string',
            'cedulaM1' => 'required|integer',
            'miembro2' => 'required|string',
            'cedulaM2' => 'required|integer'
        ];
    }
}
