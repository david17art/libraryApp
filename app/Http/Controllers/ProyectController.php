<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Proyect;
use App\ProyectMember;
use App\ProyectJury;
use App\ProyectTutor;
use Redirect;
use Auth;
use PDF;
use App\Http\Requests\JuradoRequest;
use App\Http\Requests\VeredictoRequest;
use App\Http\Requests\AprobacionRequest;
use App\Http\Requests\InvitacionRequest;
use App\Http\Requests\ProyectRequest;
use App\Http\Requests\BaremoRequest;
use App\Http\Requests\TrabajoRequest;

class ProyectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::user()->id;
        $count_proyect = Proyect::where('user_id',$id)->count();
        if ($count_proyect > 0) {
            $proyecto = Proyect::where('user_id',$id)->first()->id;
            $proyect = Proyect::where('user_id',$id)->first();
            return view('app.proyect.index',compact('id','proyecto','proyect'));
        }else{
            return Redirect::to('/app/proyecto/create');
        }
    }

    public function getProyects(){
        return Proyect::with('user','jury','member','member.user','tutor','tutor.user')->get();
    }

    public function getProyectsAll($action){
        if($action == 'all'){
            return Proyect::where('aprobe', 1)->with('user','tutor','tutor.user')->get();
        }else if($action == 'esp'){
            return Proyect::where('aprobe', 1)->with('user','tutor','tutor.user')->where('esp','si')->get();
        }else{
            return Proyect::where('aprobe', 1)->with('user','tutor','tutor.user')->where('esp','no')->get();
        }

        
    }

    public function getProyect($user){
        return Proyect::where('user_id',$user)->first();
    }

    public function getMembers($proyecto){
        return ProyectMember::where('proyect_id',$proyecto)->with('user')->get();
    }

    public function member(Request $request){
        $proyecto = Proyect::where('id',$request->proyect_id)->first();
        $proyecto->member()->create(['user_id' => $request->user_id]);
        return "Miembro creado correctamente";
    }

    public function getTutors($proyecto){
        return ProyectTutor::where('proyect_id',$proyecto)->with('user')->get();
    }
    public function tutor(Request $request){
        $proyecto = Proyect::where('id',$request->proyect_id)->first();
        $proyecto->tutor()->create(['user_id' => $request->user_id]);
        return "Tutor creado correctamente";
    }

    public function getUsers(){
        return User::where('role',1)->where('id','<>',Auth::user()->id)->get();
    }
    public function getUsersProfessor(){
        return User::where('role',2)->get();
    }

    public function getJuries($proyecto){
        return ProyectJury::where('proyect_id',$proyecto)->get();
    }
    public function jury(Request $request,$proyecto){
        $proyecto = Proyect::where('id',$proyecto)->first();
        $proyecto->jury()->create($request->all());
        return "Jurado creado correctamente";
    }

    //Actas y PDFS
    public function indexActas(){
        return view('app.proyect.actas.index');
    }
    public function formActaDeJurado(){
        $proyects = Proyect::with('user')->where('aprobe',1)->where('fase',3)->orWhere('aprobe',3)->where('fase',3)->get();
        return view('app.proyect.actas.createJurado',compact('proyects'));
    }
    public function ActaDeJurado(JuradoRequest $request){
        $proyecto = Proyect::where('id',$request->proyecto)->with('user','tutor','tutor.user')->first();
        $proyecto->fill([
            'fase' => 4
        ]);
        $proyecto->save();
        if($proyecto->tutor == null){
            abort(500);
        }else{
            $pdf = PDF::loadView('pdf.actaJurado', ['proyecto' => $proyecto,'fecha' => $request->fecha,'hora' => $request->hora,'presidente' => $request->presidente,'cedulaP' => $request->cedulaP,'miembro1' => $request->miembro1,'cedulaM1' =>$request->cedulaM1,'miembro2' => $request->miembro2,'cedulaM2' => $request->cedulaM2]);
            return $pdf->stream('jurado.pdf');
        }
        return $res;
    }
    public function formVeredicto(){
        $proyects = Proyect::with('user')->where('aprobe',1)->where('fase',4)->orWhere('aprobe',3)->where('fase',4)->get();
        return view('app.proyect.actas.createVeredicto',compact('proyects'));
    }
    public function veredicto(VeredictoRequest $request){
        $proyect = Proyect::where('id',$request->proyecto)->with('user','tutor','tutor.user')->first();
        $proyect->fill([
            'fase' => 5
        ]);
        $proyect->save();
        $pdf = PDF::loadView('pdf.actaVeredicto',['proyect' => $proyect,'presidente' => $request->presidente,'cedulaP' => $request->cedulaP,'miembro1' => $request->miembro1,'cedulaM1' =>$request->cedulaM1,'miembro2' => $request->miembro2,'cedulaM2' => $request->cedulaM2]);
        return $pdf->stream('veredicto.pdf');
    }
    public function formAprobacion(){
        $proyects = Proyect::with('user')->where('aprobe',1)->where('fase',1)->orWhere('aprobe',3)->where('fase',1)->get();
        return view('app.proyect.actas.createAprobacion',compact('proyects'));
    }
    public function aprobacion(AprobacionRequest $request){
        $proyect = Proyect::where('id',$request->proyecto)->with('user','tutor','tutor.user')->first();
        $proyect->fill([
            'fase' => 2
        ]);
        $proyect->save();
        $pdf = PDF::loadView('pdf.actaAprobacion',['proyect' => $proyect]);
        return $pdf->stream('aprobacion.pdf');
    }

    public function biblioteca($proyecto){
        $proyect = Proyect::where('id',$proyecto)->with('user','tutor','tutor.user')->first();
        $pdf = PDF::loadView('pdf.actaBiblioteca',['proyect' => $proyect]);
        return $pdf->download('biblioteca.pdf');
    }
    public function formInvitacion(){
        $proyects = Proyect::with('user')->where('aprobe',1)->where('fase',3)->orWhere('aprobe',3)->where('fase',3)->get();
        return view('app.proyect.actas.createInvitacion',compact('proyects'));  
    }
    public function invitacion(InvitacionRequest $request){
        $proyect = Proyect::where('id',$request->proyecto)->with('user','tutor','tutor.user')->first();
        $pdf = PDF::loadView('pdf.actaInvitacion',['proyect' => $proyect,'jurado' => $request->jurado]);
        return $pdf->stream('invitacion.pdf');
    }

    public function formBaremo(){
        $proyects = Proyect::with('user')->where('aprobe',1)->where('fase',1)->orWhere('aprobe',3)->where('fase',1)->get();
        return view('app.proyect.actas.createBaremo',compact('proyects'));
    }
    public function baremo(BaremoRequest $request){
        $proyecto = Proyect::where('id',$request->proyecto)->with('user','tutor','tutor.user')->first();
        $pdf = PDF::loadView('pdf.actaBaremo',['proyect' => $proyecto,
        'recomendaciones' => $request->recomendaciones,
        'generalidadesAPuntaje' => $request->generalidadesAPuntaje,
        'generalidadesAObservaciones' => $request->generalidadesAObservaciones,
        'generalidadesBPuntaje' => $request->generalidadesBPuntaje,
        'generalidadesBObservaciones' => $request->generalidadesBObservaciones,
        'generalidadesCPuntaje' => $request->generalidadesCPuntaje,
        'generalidadesCObservaciones' => $request->generalidadesCObservaciones,
        'tituloAPuntaje' => $request->tituloAPuntaje,
        'tituloAObservaciones' => $request->tituloAObservaciones,
        'tituloBPuntaje' => $request->tituloBPuntaje,
        'tituloBObservaciones' => $request->tituloBObservaciones,
        'tituloCPuntaje' => $request->tituloCPuntaje,
        'tituloCObservaciones' => $request->tituloCObservaciones,
        'tituloDPuntaje' => $request->tituloDPuntaje,
        'tituloDObservaciones' => $request->tituloDObservaciones,
        'problemaAPuntaje' => $request->problemaAPuntaje,
        'problemaAObservaciones' => $request->problemaAObservaciones,
        'problemaBPuntaje' => $request->problemaBPuntaje,
        'problemaBObservaciones' => $request->problemaBObservaciones,
        'problemaCPuntaje' => $request->problemaCPuntaje,
        'problemaCObservaciones' => $request->problemaCObservaciones,
        'problemaDPuntaje' => $request->problemaDPuntaje,
        'problemaDObservaciones' => $request->problemaDObservaciones,
        'problemaEPuntaje' => $request->problemaEPuntaje,
        'problemaEObservaciones' => $request->problemaEObservaciones,
        'problemaFPuntaje' => $request->problemaFPuntaje,
        'problemaFObservaciones' => $request->problemaFObservaciones,
        'marcoAPuntaje' => $request->marcoAPuntaje,
        'marcoAObservaciones' => $request->marcoAObservaciones,
        'marcoBPuntaje' => $request->marcoBPuntaje,
        'marcoBObservaciones' => $request->marcoBObservaciones,
        'marcoCPuntaje' => $request->marcoCPuntaje,
        'marcoCObservaciones' => $request->marcoCObservaciones,
        'marcoDPuntaje' => $request->marcoDPuntaje,
        'marcoDObservaciones' => $request->marcoDObservaciones,
        'marcoEPuntaje' => $request->marcoEPuntaje,
        'marcoEObservaciones' => $request->marcoEObservaciones,
        'marcoFPuntaje' => $request->marcoFPuntaje,
        'marcoFObservaciones' => $request->marcoFObservaciones,
        'metodologiaAPuntaje' => $request->metodologiaAPuntaje,
        'metodologiaAObservaciones' => $request->metodologiaAObservaciones,
        'metodologiaBPuntaje' => $request->metodologiaBPuntaje,
        'metodologiaBObservaciones' => $request->metodologiaBObservaciones,
        'metodologiaCPuntaje' => $request->metodologiaCPuntaje,
        'metodologiaCObservaciones' => $request->metodologiaCObservaciones,
        'metodologiaDPuntaje' => $request->metodologiaDPuntaje,
        'metodologiaDObservaciones' => $request->metodologiaDObservaciones,
        'metodologiaEPuntaje' => $request->metodologiaEPuntaje,
        'metodologiaEObservaciones' => $request->metodologiaEObservaciones,
        'metodologiaFPuntaje' => $request->metodologiaFPuntaje,
        'metodologiaFObservaciones' => $request->metodologiaFObservaciones,
        'aspectosAPuntaje' => $request->aspectosAPuntaje,
        'aspectosAObservaciones' => $request->aspectosAObservaciones,
        'aspectosBPuntaje' => $request->aspectosBPuntaje,
        'aspectosBObservaciones' => $request->aspectosBObservaciones,
        'aspectosCPuntaje' => $request->aspectosCPuntaje,
        'aspectosCObservaciones' => $request->aspectosCObservaciones,
        'aspectosDPuntaje' => $request->aspectosDPuntaje,
        'aspectosDObservaciones' => $request->aspectosDObservaciones,
        'aspectosEPuntaje' => $request->aspectosEPuntaje,
        'aspectosEObservaciones' => $request->aspectosEObservaciones,
        'formaAPuntaje' => $request->formaAPuntaje,
        'formaAObservaciones' => $request->formaAObservaciones,
        'formaBPuntaje' => $request->formaBPuntaje,
        'formaBObservaciones' => $request->formaBObservaciones,
        'formaB1Puntaje' => $request->formaB1Puntaje,
        'formaB1Observaciones' => $request->formaB1Observaciones,
        'formaB2Puntaje' => $request->formaB2Puntaje,
        'formaB2Observaciones' => $request->formaB2Observaciones,
        'formaB3Puntaje' => $request->formaB3Puntaje,
        'formaB3Observaciones' => $request->formaB3Observaciones,
        'formaCPuntaje' => $request->formaCPuntaje,
        'formaCObservaciones' => $request->formaCObservaciones,
        ])->setPaper('a4', 'landscape');
        return $pdf->stream('baremo.pdf');
    }
    public function test($proyecto){
        $proyecto = Proyect::where('id',$proyecto)->with('user','tutor','tutor.user')->first();
        return view('pdf.actaJurado',compact('proyecto'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('app.proyect.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getFormTrabajo($proyect){
        $proyecto = Proyect::findOrFail($proyect);
        $id = $proyecto->id;
        return view('app.proyect.createT',compact('id'));
    }

    public function trabajo($proyect,TrabajoRequest $request){
        $proyecto = Proyect::findOrFail($proyect);
        $proyecto->type = $request->type;
        $proyecto->fase = 3;
        $file = $request->file('file2');
        $file_name = str_random(6).".".$file->getClientOriginalExtension();
        $file->move("proyectos",$file_name);
        $proyecto->file2 = "proyectos/".$file_name;
        $proyecto->save();
        return Redirect::to('/app/proyecto');
    }
    public function store(ProyectRequest $request)
    {
        $file = $request->file('file');
        $file_name = str_random(6).".".$file->getClientOriginalExtension();
        $file->move("proyectos",$file_name);
        $user = Auth::user();
        $user->proyect()->create([
            "title" => $request->title,
            "description" => $request->description,
            "file" => "proyectos/".$file_name,
            "aprobe" => 0,
            "linea" => $request->linea,
            "type" => "P",
            "grado" => $request->grado,
            "programa" => $request->programa
        ]);
        return Redirect::to('/app/proyecto');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $proyecto = Proyect::findOrFail($id);
        return view('app.proyect.show',compact('proyecto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $proyect = Proyect::findOrFail($id);
        $proyect->fill($request->all());
        $proyect->save();
        return response()->json(["message" => "Tesis Correctamente aprobada"]);
    }

    public function aprobe(Request $request,$id){
        $proyect = Proyect::findOrFail($id);
        if($request->aprobe == 1 || $request->aprobe == 3){
            $proyect->fill([
                "aprobe" => $request->aprobe,
                "fase" => 1
            ]);
        }else{
            $proyect->fill($request->all());
        }
        $proyect->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (ProyectMember::where('proyect_id',$id)->count() > 0) {
            $members = ProyectMember::where('proyect_id',$id)->get();
            foreach($members As $member){
                $idP = $member->id;
                ProyectMember::destroy($idP);
            }
        }
        if (ProyectTutor::where('proyect_id',$id)->count() > 0) {
            $members = ProyectTutor::where('proyect_id',$id)->get();
            foreach($tutos As $tuto){
                $idT = $tuto->id;
                ProyectTutor::destroy($idT);
            }
        }   
        if (ProyectJury::where('proyect_id',$id)->count() > 0) {
            $juries = ProyectJury::where('proyect_id',$id)->get();
            foreach($juries As $jury){
                $idT = $jury->id;
                ProyectJury::destroy($idT);
            }
        }                
        Proyect::destroy($id);

        return "Correctamente eliminado";
    }
}
