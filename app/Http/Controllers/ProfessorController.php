<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use App\User;

class ProfessorController extends Controller
{
	public function registerForm(){
		return view('auth.registerProfessor');
	}
	public function register(Request $request){
		User::create([
			"name" => $request->name,
			"email" => $request->email,
			"cedula" => $request->cedula,
			"role" => 2,
			"password" => bcrypt($request->password)

		]);

		return Redirect::to("login");
	}
}
