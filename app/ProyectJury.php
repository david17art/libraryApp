<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProyectJury extends Model
{
	protected $table = 'proyect_juries';
	protected $fillable = ['name'];
    public function proyect(){
        return $this->belongsTo(Proyect::class);
    }  
}
