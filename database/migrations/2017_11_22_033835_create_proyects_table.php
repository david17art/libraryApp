<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProyectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->text('title');
            $table->text('description');
            $table->string('linea');
            $table->string('grado');
            $table->string('programa');
            $table->string('file');
            $table->string('file2')->nullable();
            $table->enum('type',["TG","TEG","P"]);
            $table->integer('aprobe');
            $table->enum('esp',['si','no']);
            $table->integer('fase')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proyects');
    }
}
