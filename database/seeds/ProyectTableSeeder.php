<?php

use Illuminate\Database\Seeder;
use App\Proyect;
use App\User;

class ProyectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $proyect = User::findOrFail(5)->proyect()->create([
            'title' => "Proyecto creado automaticamente numero 1",
            'description' => 'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen.',
            'file' => 'proyectos/3L7ncW.pdf',
            'esp' => 'si',
            'grado' => 'Grado especial de ingenieria',
            'type' => 'TEG',
            'programa' => "Programa aleatorio de la universidad Jose Antonio Paez",
            'linea' => "Linea de investigacion completamente aleatoria",
            'aprobe' => 0
        ]);
        $proyect->tutor()->create([
            'user_id' => 2
        ]);
        $proyect = User::findOrFail(6)->proyect()->create([
            'title' => "Proyecto creado automaticamente numero 2",
            'description' => 'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen.',
            'file' => 'proyectos/Av4fjz.pdf',
            'esp' => 'no',
            'grado' => 'Grado especial de derecho',
            'type' => 'TG',
            'programa' => "Programa aleatorio de la universidad Jose Antonio Paez",
            'linea' => "Linea de investigacion completamente aleatoria",
            'aprobe' => 2,
        ]);
        $proyect->tutor()->create([
            'user_id' => 3
        ]);
        $proyect =User::findOrFail(7)->proyect()->create([
            'title' => "Proyecto creado automaticamente numero 3",
            'description' => 'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen.',
            'file' => 'proyectos/Av4fjz.pdf',
            'esp' => 'no',
            'grado' => 'Grado especial de mercadeo',
            'type' => 'P',
            'programa' => "Programa aleatorio de la universidad Jose Antonio Paez",
            'linea' => "Linea de investigacion completamente aleatoria",
            'aprobe' => 0
        ]);
        $proyect->tutor()->create([
            'user_id' => 4
        ]);
        $proyect = User::findOrFail(8)->proyect()->create([
            'title' => "Proyecto creado automaticamente numero 4",
            'description' => 'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen.',
            'file' => 'proyectos/Av4fjz.pdf',
            'esp' => 'si',
            'grado' => 'Grado especial de Literatura',
            'type' => 'TG',
            'programa' => "Programa aleatorio de la universidad Jose Antonio Paez",
            'linea' => "Linea de investigacion completamente aleatoria",
            'aprobe' => 1,
            'fase' => 1
        ]);
        $proyect->tutor()->create([
            'user_id' => 2
        ]);
    }
}
