
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));
Vue.component('proyects',require('./components/proyects/index.vue'))
Vue.component('proyect',require('./components/proyects/show.vue'))
Vue.component('members',require('./components/proyects/members'));
Vue.component('juries',require('./components/proyects/juries.vue'));
Vue.component('tutor',require('./components/proyects/tutor.vue'));
Vue.component('students',require('./components/proyects/students.vue'));

const app = new Vue({
    el: '#app'
});
