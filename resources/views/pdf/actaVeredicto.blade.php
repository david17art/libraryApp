<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>VEREDICTO</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<style>
	body{
		
	}
	.parrafo {
		text-align:justify;
		font-size: 12pt;
		line-height: normal;
	}
	.wrapper{
		margin-right: 4rem;
		margin-left: 4rem;
	}
	.header{
		margin-right: 4rem;
		margin-left:4rem;
		display: inline-table;
	}
	.space{
		margin-right: 7.5rem;
	}
	.box{
		border: 2px solid #000;
		padding: 20px 20px 20px 20px;
	}
	.pie{
		position:absolute;
		bottom:0;
	}
	.text-center{
		text-align:center;
	}
</style>

</head>
<body>
	<table align="center">
		<tr>
			<th></th>
			<th><img src="https://upload.wikimedia.org/wikipedia/commons/6/62/Logo-UJAP2.jpg" width="150px" alt=""></th>
			<th></th>
		</tr>
		<tr>
			<th></th>
			<th><h4 style="text-align:center;">UNIVERSIDAD JOSÉ ANTONIO PÁEZ <br>
				VICERRECTORADO ACADEMICO <br>
				DIRECCIÓN GENERAL DE ESTUDIOS DE POSTGRADO
			</h4></th>
			<th></th>
		</tr>
	</table>

	<h3 class="text-center">VEREDICTO</h3>
	<p class="parrafo">
		Nosotros, miembros del jurado designado para la evaluacion del @if($proyect->type == 'TG') Trabajo de Grado @elseif($proyect->type == 'TEG') Trabajo Especial de Grado @else Proyecto @endif presentado por el ciudadano <strong>{{$proyect['user']->name}}</strong> cedula de identidad Nº <strong>{{$proyect['user']->cedula}},</strong> titulado: <strong>"{{$proyect->title}}",</strong> elaborado bajo la supervisión de la tutora <strong>{{$proyect['tutor']['user']->name}},</strong> cedula de identidad Nº <strong>{{$proyect['tutor']['user']->cedula}}, </strong> abscrito a la linea de investigacion: {{$proyect->linea}}, para optar al grado academico de {{$proyect->grado}}, estimamos que el mismo reune los requisitos para ser considerado como <strong>APROBADO.</strong>
	</p> <br><br>
	<table align="center">
		<thead>
			<tr>
				<th>Nombre, Apellido</th>
				<th>CI</th>
				<th>Firma de Jurado</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>{{$presidente}}(presidente)</td>
				<td>{{$cedulaP}}</td>
				<td>_________________________________</td>
			</tr>
			<tr>
				<td>{{$miembro1}} (miembro)</td>
				<td>{{$cedulaM1}}</td>
				<td>_________________________________</td>
			</tr>
			<tr>
				<td>{{$miembro2}} (miembro)</td>
				<td>{{$miembro2}}</td>
				<td>_________________________________</td>
			</tr>						
		</tbody>
	</table>

	<p class="pie" style="text-align: center;">Urb. Yuma II, calle Nº 3, Municipio San Diego. Carabobo, Venezuela, Telefonos: (0241) 8714240(Master)  8710903 postgrado@ujap.edu.ve</p>
</body>

</html>