<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ACTA DE APROBACION DE PROYECTO  Y DESIGNACION DE TUTORA</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<style>
	body{
		
	}
	.parrafo {
		text-align:justify;
		font-size: 12pt;
		line-height: normal;
	}
	.wrapper{
		margin-right: 1rem;
		margin-left: 1rem;
	}
	.header{
		margin-right: 4rem;
		margin-left:4rem;
		display: inline-table;
	}
	.space{
		margin-right: 7.5rem;
	}
	.box{
		border: 2px solid #000;
		padding: 20px 20px 20px 20px;
	}
    .table{
        border:1px solid black;
    }
    .table > thead > tr > th{
        border:1px solid black;
    }
    .table > tbody > tr > td{
        border:1px solid black;
    }    
</style>

</head>
<body>
<div class="wrapper">
	<table>
		<tr>
			<th><img src="https://upload.wikimedia.org/wikipedia/commons/6/62/Logo-UJAP2.jpg" width="150px" alt=""></th>
			<th class="text-center">
				REPUBLICA BOLIVARIANA DE VENEZUELA <br>
                UNIVERSIDAD JOSÉ ANTONIO PAEZ <br>
                DIRECCION GENERAL DE ESTUDIOS DE POSTGRADO
			</th>
		</tr>
	</table>
	<br>
	<br>
	
	<h3>BAREMO PARA LA EVALUACION DEL PROYECTO DE TRABAJO ESPECIAL DE GRADO Y TRABAJO DE GRADO</h3>
    <table align="left">
        <tr>
            <th>Fecha: {{ Carbon\Carbon::now()->format('Y-m-d') }}</th>
        </tr>
    </table>
    <br>
    <br>
    <table align="center" class="table" width="100%">
        <thead>
            <tr>
                <th colspan="2">Programa: {{$proyect->programa}}</th>
            </tr>
            <tr>
                <th>Estudiante: {{$proyect['user']->name}}</th>
                <th>CI: {{$proyect['user']->cedula}}</th>
            </tr>
        </thead>
    </table>
    <br>
    <br>
    <table align="center" class="table" width="100%">
        <thead>
            <tr>
                    <th colspan="2">Titulo: {{$proyect->title}}</th>
            </tr>
            <tr>
                    <th colspan="2">Line de investigacion: {{$proyect->linea}}</th>
            </tr>         
            <tr>
                <th>Tutor Propuesto: {{$proyect['tutor']['user']->name}}</th>
                <th>CI: {{$proyect['tutor']['user']->cedula}}</th>
            </tr>
        </thead>
    </table>
    <br><br>
    <table align="center" class="table" width="100%">
        <thead>
            <tr>
                <th>Recomendaciones: {{$recomendaciones}}</th>
            </tr>
        </thead>
    </table>
    <br>
    <table align="center" class="table" width="100%">
        <thead>
            <tr>
                <th>SECCIÓN</th>
                <th>ASPECTOS A EVALUAR</th>
                <th>PUNTAJE</th>
                <th>OBSERVACIONES</th>
            </tr>
        </thead>
        <tbody>

            <tr>
                    <td rowspan="3">Generalidades</td>
                    <td>a) El proyecto se ajusta a las normas vigentes del manual para la elaboracion del proyecto y trabajo final de gradode la direccion general de postgrado de la Universidad Jose Antonio Paez</td>
                    <td>{{$generalidadesAPuntaje}}</td>
                    <td>{{$generalidadesAObservaciones}}</td>
                </tr>
                <tr>
                    <td>b) El proyecto está abscrito a una linea de investigación del Programa.</td>
                    <td>{{$generalidadesBPuntaje}}</td>
                    <td>{{$generalidadesBObservaciones}}</td>
                </tr>
                <tr>
                        <td>c) El resumen contiene: una breve introduccion, el objetivo general, la metodologia y posibles resultados o conclusiones; el texto está presentado en un solo bloque y no excede 300 palabras.</td>
                        <td>{{$generalidadesCPuntaje}}</td>
                        <td>{{$generalidadesCObservaciones}}</td>
                </tr>            
                <tr>
                        <td rowspan="4"> <strong>I. Titulo</strong> </td>
                        <td>a) Da una visión global, concisa y clara del ambito de estudio o problema de investigacion</td>
                        <td>{{$tituloAPuntaje}}</td>
                        <td>{{$tituloAObservaciones}}</td>
                    </tr>
                    <tr>
                        <td>b) Especifica el tiempo y espacio en que se realizará el estudio.</td>
                        <td>{{$tituloBPuntaje}}</td>
                        <td>{{$tituloBObservaciones}}</td>
                    </tr>
                    <tr>
                            <td>c) Se relaciona con el objetivo general y el contenido del trabajo.</td>
                        <td>{{$tituloCPuntaje}}</td>
                        <td>{{$tituloCObservaciones}}</td>
                    </tr>
                    <tr>
                            <td>d) Indica el área de conocimiento.</td>
                            <td>{{$tituloDPuntaje}}</td>
                            <td>{{$tituloDObservaciones}}</td>
                    </tr>                   
                    <tr>
                            <td rowspan="6"> <strong>II. PROBLEMA O ÁMBITO DE ESTUDIO</strong> </td>
                            <td>a) Está claramente planteado en cuanto a descripcion, causas y consecuencias. Además es congruente con el objetivo general y la linea de investigacion</td>
                            <td>{{$problemaAPuntaje}}</td>
                            <td>{{$problemaAObservaciones}}</td>
                        </tr>
                        <tr>
                            <td>b) Los supuestos implícitos o las hipótesis (si es el caso) están claramente planteados. Si es el caso, reflejan las variables independientes y dependientes, y las mismas están operacionalizadas.</td>
                            <td>{{$problemaBPuntaje}}</td>
                            <td>{{$problemaBObservaciones}}</td>
                        </tr>
                        <tr>
                                <td>c) El problema es relevante, Aporta una alternativa de solución a nivel nacional, estadal, municipal, local a organizaciones públicas y/o privadas.</td>
                                <td>{{$problemaCPuntaje}}</td>
                                <td>{{$problemaCObservaciones}}</td>
                        </tr>
                        <tr>
                                <td>d) Los objetivos específicos están relacionados con el objetivo general.</td>
                                <td>{{$problemaDPuntaje}}</td>
                                <td>{{$problemaDObservaciones}}</td>
                        </tr>
                        <tr>
                                <td>e) Los objetivos expresan las interrogantes a responder o aspectos desconocidos a dilucidar.</td>
                                <td>{{$problemaEPuntaje}}</td>
                                <td>{{$problemaEObservaciones}}</td>
                        </tr>
                        <tr>
                                <td>f) La justificacion explica el porqué y para qué se realizará el estudio, su importancia, posibles contribuciones y aportes.</td>
                                <td>{{$problemaFPuntaje}}</td>
                                <td>{{$problemaFObservaciones}}</td>
                        </tr> 
                        <tr>
                                <td rowspan="6"> <strong>III. MARCO TEORICO REFERENCIAL / REVISION DE LITERATURA</strong> </td>
                                <td>a) Se relacionan los antecedentes de la investigacion con el problema planteado o ámbito de estudio.</td>
                                <td>{{$marcoAPuntaje}}</td>
                                <td>{{$marcoAObservaciones}}</td>
                            </tr>
                            <tr>
                                    <td>b) Presenta un cuerpo teórico pertinente y actualizado con relacion al objeto/sujeto de estudio y a los objetivos de investigación, contiene un número importante de referencias a revistas y/o publicaciones periódicas del área especifica de la temática que se investiga.</td>
                                    <td>{{$marcoBPuntaje}}</td>
                                    <td>{{$marcoBObservaciones}}</td>
                            </tr>
                            <tr>
                                    <td>c) Presenta un marco teórico bien organizado y pertinente.</td>
                                    <td>{{$marcoCPuntaje}}</td>
                                    <td>{{$marcoCObservaciones}}</td>
                            </tr>
                            <tr>
                                    <td>d) Demuestra el conocimiento del investigador sobre los enfoques y tendencias actuales en cuanto al objeto / sujeto de estudio.</td>
                                    <td>{{$marcoDPuntaje}}</td>
                                    <td>{{$marcoDObservaciones}}</td>
                            </tr>
                            <tr>
                                    <td>e) Estan definidos operacionales los terminos innovadores, relevantes y excepciones; Si es pertinente.</td>
                                    <td>{{$marcoEPuntaje}}</td>
                                    <td>{{$marcoEObservaciones}}</td>
                            </tr>
                            <tr>
                                    <td>f) La bibliografia está actualizada; es decir, no ha caducado ni tiene mas de cinco(05) años.</td>
                                    <td>{{$marcoFPuntaje}}</td>
                                    <td>{{$marcoFObservaciones}}</td>
                            </tr>
                            <tr>
                                    <td rowspan="6"> <strong>IV. METODOLOGIA</strong> </td>
                                    <td>a) Se describen claramente el tipo, los metodos y nivel de investigacion.</td>
                                    <td>{{$metodologiaAPuntaje}}</td>
                                    <td>{{$metodologiaAObservaciones}}</td>
                                </tr>
                                <tr>
                                        <td>b) Se descruveb claramente el diseño de investigacion y las tecnicas e instrumentos de recoleccion de datos.</td>
                                        <td>{{$metodologiaBPuntaje}}</td>
                                        <td>{{$metodologiaBObservaciones}}</td>
                                </tr>
                                <tr>
                                        <td>c) El diseño de la investigacion es apropiado para abordar la alternativa de solucion propuesta y describe los procedimientos requeridos.</td>
                                        <td>{{$metodologiaCPuntaje}}</td>
                                        <td>{{$metodologiaCObservaciones}}</td>
                                </tr>
                                <tr>
                                        <td>d) Si es el caso, la operacionalizacion de las variables conduce a la elaboracion de instrumento que se va a aplicar.</td>
                                        <td>{{$metodologiaDPuntaje}}</td>
                                        <td>{{$metodologiaDObservaciones}}</td>
                                </tr>
                                <tr>
                                        <td>e) Las tecnicas o preocedimientos para analizar la informacion son apropiados para el tipo de investigacion que se va a realizar.</td>
                                        <td>{{$metodologiaEPuntaje}}</td>
                                        <td>{{$metodologiaEObservaciones}}</td>
                                </tr>
                                <tr>
                                        <td>f) Se describe la poblacion y muestra) informantes claves segun el enfoque de investigacion seleccionado.</td>
                                        <td>{{$metodologiaFPuntaje}}</td>
                                        <td>{{$metodologiaFObservaciones}}</td>
                                </tr>   
                                <tr>
                                    <td rowspan="5"> <strong>V. ASPECTOS ADMINISTRATIVOS</strong> </td>
                                    <td>a) Dispone el investigador de la formacion teorica y cientifica para abordar el problema; y ademas dispone de la infraestructura tecnologica necesaria para desarrollar el proyecto.</td>
                                    <td>{{$aspectosAPuntaje}}</td>
                                    <td>{{$aspectosAObservaciones}}</td>
                                </tr>
                                <tr>
                                        <td>b) Evidencia disponer de la infraestructura y logistica adecuada para desarrollar el proyecto.</td>
                                        <td>{{$aspectosBPuntaje}}</td>
                                        <td>{{$aspectosBObservaciones}}</td>
                                </tr>
                                <tr>
                                        <td>c) Presenta el investigador los recursos financieros bien sean propios o de subvencion, necesarios para cubrir los costos del proyecto.</td>
                                        <td>{{$aspectosCPuntaje}}</td>
                                        <td>{{$aspectosCObservaciones}}</td>
                                </tr>
                                <tr>
                                        <td>d) Relaciona el personal necesario para realizar la investigacion.</td>
                                        <td>{{$aspectosDPuntaje}}</td>
                                        <td>{{$aspectosDObservaciones}}</td>
                                </tr>
                                <tr>
                                        <td>e) Presenta un cronograma de actividades organizado por tareas indicando los lapsos de ejecución.</td>
                                        <td>{{$aspectosEPuntaje}}</td>
                                        <td>{{$aspectosEObservaciones}}</td>
                                </tr> 
                                <tr>
                                        <td rowspan="6"> <strong>VI. FORMA Y ESTILO</strong> </td>
                                        <td>a) Uso del lenguaje cientifico: precision, uso de la terminologia pertinente.</td>
                                        <td>{{$formaAPuntaje}}</td>
                                        <td>{{$formaAObservaciones}}</td>
                                    </tr>
                                    <tr>
                                            <td>b) Uso de lenguaje materno.</td>
                                            <td>{{$formaBPuntaje}}</td>
                                            <td>{{$formaBObservaciones}}</td>
                                    </tr>
                                    <tr>
                                            <td>    b.1 El proyecto está claramente redactado.</td>
                                            <td>{{$formaB1Puntaje}}</td>
                                            <td>{{$formaB1Observaciones}}</td>
                                    </tr> 
                                    <tr>
                                            <td>    b.2 Utiliza una gramatica y lenguaje apropiados.</td>
                                            <td>{{$formaB2Puntaje}}</td>
                                            <td>{{$formaB2Observaciones}}</td>
                                    </tr>  
                                    <tr>
                                            <td>    b.3 Revela una buena ortografia.</td>
                                            <td>{{$formaB3Puntaje}}</td>
                                            <td>{{$formaB3Observaciones}}</td>
                                    </tr>                                                                        
                                    <tr>
                                            <td>c) Uso correcto de las Normas APA.</td>
                                            <td>{{$formaCPuntaje}}</td>
                                            <td>{{$formaCObservaciones}}</td>
                                    </tr>                                                                                                                                                                                                                       
        </tbody>
    </table>

    <br>
    <table class="table" width="100%">
        <thead>
            <tr>
                <th>
                    Apellidos, Nombre
                </th>
                <th>
                        Cedula de Identidad
                </th> 
                <th>
                    Firma    
                </th>            
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{$proyect['user']->name}}</td>
                <td>{{$proyect['user']->cedula}}</td>
                <td></td>
            </tr>                          
        </tbody>
    </table>
</div>
</body>

</html>