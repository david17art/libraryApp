<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Solvencia Academica</title>
<style>
	body{
		margin: 4rem 4rem 4rem 4rem;
	}
	.text-justify{
		text-align:justify;
	}
	.text-center{
		text-align:center;
	}
</style>
</head>
<body>	
	<table class="table" align="center">
		<thead>
			<tr>
				<th> <h3 class="text-center">Solvencia Academica</h3> </th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>
					<p class="text-justify">Se hace constar que el(la) ciudadano(a) <strong>{{$proyect['user']->name}}</strong> portador de la Cedula de identidad Nº <strong>{{$proyect['user']->cedula}}</strong> , consigno el ejemplar digital de su Trabajo de Grado/ Trabajo Especial de Grado titulado: <strong>{{$proyect->title}}</strong> en formato .PDF cargado a través de la Web.</p>

					<br><br>
					<p style="text-align: right;">{{$proyect->created_at}}, firma y sello</p>
				</td>
			</tr>
		</tbody>
	</table>
</body>
</html>