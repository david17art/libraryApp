<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ACTA DE CONSTITUCION DE JURADO</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<style>
	body{
		
	}
	.parrafo {
		text-align:justify;
		font-size: 12pt;
		line-height: normal;
	}
	.wrapper{
		margin-right: 4rem;
		margin-left: 4rem;
	}
	.header{
		margin-right: 4rem;
		margin-left:4rem;
		display: inline-table;
	}
	.space{
		margin-right: 7.5rem;
	}
	.box{
		border: 2px solid #000;
		padding: 20px 20px 20px 20px;
	}
</style>

</head>
<body>
		<table align="center">
			<tr>
				<th><img src="https://upload.wikimedia.org/wikipedia/commons/6/62/Logo-UJAP2.jpg" width="150px" alt=""></th>
			<th>
				<div class="box">
					UNIVERSIDAD JOSE ANTONIO PAEZ <br>
					VICERRECTORADO ACADEMICO <br>
					DIRECCIÓN GENERAL DE ESTUDIO DE POSTGRADO <br>
				</div>			
			</th>
			</tr>
		</table>		
<div class="wrapper">
	<h2 style="text-align:center">ACTA DE CONSTITUCIÓN DE JURADO</h2> <br>
<p class="parrafo">
En atención a lo establecido en el Articulo 87 del Reglamento de Estudio de Postgrado de la
Universidad José Antonio Paéz, nosotros miembro del Jurado designados por el Consejo de Estudios
de Postgrado para evaluar el Trabajo Especial de Grado/Trabajo de Grado elaborado por el (la)
ciudadano (a) , <strong>{{$proyecto["user"]->name}}</strong>
titular de la cédula de identidad No. <strong>{{$proyecto["user"]->cedula}}</strong>, titulado
<strong>{{$proyecto->title}}</strong> adscrito al programa {{$proyecto->programa}}
en la línea de investigación: <strong>{{$proyecto->linea}}</strong>
bajo la tutoría de <strong>{{$proyecto["tutor"]["user"]->name}}</strong>,
titular de la cedula de identidad No. <strong>{{$proyecto["tutor"]["user"]->cedula}}</strong> nos damos por constituidos
en fecha <strong>{{ Carbon\Carbon::now()->format('Y-m-d') }}</strong> y acordamos convocar al (la) precipitado (a) ciudadano (a), para
la fecha <strong>{{$fecha}}</strong>, hora <strong>{{$hora}}</strong>
</p>
<br><br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<table align="center">
	<thead>
		<tr>
			<th align="center">Presidente</th>
			<th align="center">Miembro</th>
			<th align="center">Miembro</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>
				Profesor {{$presidente}} <br>
				C.I {{$cedulaP}}
			</td>
			<td>
				Profesor {{$miembro1}} <br>
				C.I {{$cedulaM1}}
			</td>
			<td>
				Profesor {{$miembro2}} <br>
				C.I {{$cedulaM2}}
			</td>			
		</tr>
	</tbody>
</table>
<br><br>
<strong>Firma y Cedula de Identidad del Estudiante</strong> ___________________________
<br>
@if($proyecto->aprobe == 1)
	<input type="checkbox" checked>  Aprobado <br>
	<input type="checkbox"> Aprobado con observaciones <br>
	<input type="checkbox">  Reprobado <br>	
@else
	<input type="checkbox">  Aprobado <br>
	<input type="checkbox" checked> Aprobado con observaciones <br>
	<input type="checkbox">  Reprobado <br>
@endif


Fecha: ________________
<p style="text-align:justify;">
	OBSERVACIONES__________________________________________________________
___________________________________________________________________________
___________________________________________________________________________

</p>
</div>
</body>
</html>