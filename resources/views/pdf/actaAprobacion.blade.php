<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ACTA DE APROBACION DE PROYECTO  Y DESIGNACION DE TUTORA</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<style>
	body{
		
	}
	.parrafo {
		text-align:justify;
		font-size: 12pt;
		line-height: normal;
	}
	.wrapper{
		margin-right: 4rem;
		margin-left: 4rem;
	}
	.header{
		margin-right: 4rem;
		margin-left:4rem;
		display: inline-table;
	}
	.space{
		margin-right: 7.5rem;
	}
	.box{
		border: 2px solid #000;
		padding: 20px 20px 20px 20px;
	}
</style>

</head>
<body>
<div class="wrapper">
	<table>
		<tr>
			<th><img src="https://upload.wikimedia.org/wikipedia/commons/6/62/Logo-UJAP2.jpg" width="150px" alt=""></th>
			<th>
				UNIVERSIDAD JOSÉ ANTONIO PÁEZ <br>
				VICERRECTORADO ACADEMICO <br>
				DIRECCION GENERAL DE ESTUDIOS DE POSTGRADO 
			</th>
		</tr>
	</table>
	<br>
	<br>

	<p style="text-align:left;">San diego {{ Carbon\Carbon::now()->format('Y-m-d') }}</p>
	<p style="text-align: right;">ETLC-{{$proyect->created_at}}</p>
	
	<h3>ACTA DE APROBACION DE PROYECTO Y DESIGNACION DE TUTORA</h3>

	<P class="parrafo">
		Los miembros de la comision de @if($proyect->type == 'TG') Trabajo de Grado @elseif($proyect->type == 'TEG') Trabajo Especial de Grado @else Proyecto @endif del programa de {{$proyect->programa}} en cumplimiento de la atribucion establecida en el articulo 15 del reglamento de estudios de postgrado de la Universidad José Antonio Paéz, en reunion celebrada en fecha de {{ Carbon\Carbon::now()->format('Y-m-d') }} acordaron: <strong>PRIMERO:</strong> @if($proyect->type == 'TG') Trabajo de Grado @elseif($proyect->type == 'TEG') Trabajo Especial de Grado @else Proyecto @endif titulado  <strong>{{$proyect->title}} ,</strong> adscrito a la linea de investigacion <strong>{{$proyect->linea}}</strong> elaborado por el ciudadano <strong>{{$proyect['user']->name}}</strong> Titular de la cedula de identidad <strong>Nº {{$proyect['user']->cedula}}</strong>  con la previa aceptacion de la tutora propuesta, ciudadana, Profesor <strong>{{$proyect['tutor']['user']->name}},</strong> titular de la  cedula de identidad Nº <strong>{{$proyect['tutor']['user']->cedula}}, SEGUNDO: </strong> dada la evaluacion favorable de las credenciales presentadas ante esta comision, solicitar su designacion formal como tutora por parte de la Direccion General de Estudios de Postgrado.
		<br><br>
		En cumplimiento de sus atribuciones reglamentarias, la dereccion general de estudios de postgrado designa a la ciudadana, Profesor <strong>{{$proyect['tutor']['user']->name}},</strong> titular de la  cedula de identidad Nº <strong>{{$proyect['tutor']['user']->cedula}}, </strong> como tutora del @if($proyect->type == 'TG') Trabajo de Grado @elseif($proyect->type == 'TEG') Trabajo Especial de Grado @else Proyecto @endif antes identificado
	</P>
	<br><br><br>
	<table align="center">
		<tr>
			<th>
				____________________________________ <br>
				<p style="text-align:center;">
					Dra. Eddy Riera de Montero <br>
					Directora General de Estudios de Post grado
				</p>
			</th>
			<th>
				____________________________________ <br>
				<p style="text-align:center;">
					Esp. Milagros Mejias <br>
					Coordinadora Comision de Trabajo de Grado
				</p>
			</th>			
		</tr>
	</table>
</div>
</body>

</html>