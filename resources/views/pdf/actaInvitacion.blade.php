<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ACTA DE INVITACION AL JURADO</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>	
	<style>
	body{
		
	}
	.parrafo {
		text-align:justify;
		font-size: 12pt;
		line-height: normal;
	}
	.wrapper{
		margin-right: 4rem;
		margin-left: 4rem;
	}
	.header{
		margin-right: 4rem;
		margin-left:4rem;
		display: inline-table;
	}
	.space{
		margin-right: 7.5rem;
	}
	.box{
		border: 2px solid #000;
		padding: 20px 20px 20px 20px;
	}
	.text-left{
		text-align: left;
	}
	.text-right{
		text-align: right;
	}
	.text-center{
		text-align: center;
	}
</style>
</head>
<body>
	<table>
		<tr>
			<th><img src="https://upload.wikimedia.org/wikipedia/commons/6/62/Logo-UJAP2.jpg" width="150px" alt=""></th>
			<th>
				UNIVERSIDAD JOSÉ ANTONIO PÁEZ <br>
				VICERRECTORADO ACADEMICO <br>
				DIRECCION GENERAL DE ESTUDIOS DE POSTGRADO 
			</th>
		</tr>
	</table>
		<div class="wrapper">
			<p class="text-right">San Diego {{ Carbon\Carbon::now()->format('Y-m-d') }}</p>
			<p class="text-left">Ciudadano(a) <br>
				{{$jurado}}
			</p>
			<p class="parrafo">
				En atención a lo establecido en los articulos 6,10,13 y 85 del Reglamento de Estudios de Postgrado, es grato dirigirme a usted en la oportunidad de informarle que el Consejo General de Estudios de Postgrado, en su sesión ordinaria Nº 89 de fecha {{ Carbon\Carbon::now()->format('Y-m-d') }} aprobó su designacion como <strong>Miembro, </strong> del jurado examinador integrado además por las profesoras: Florangel Ortiz <strong>(Presidente)</strong> Belkis Araujo y Yandira Páez <strong>(Miembro).</strong> para evaluar el Trabajo de Grado titulado: <strong>" {{$proyect->title}} "</strong> inscrito en la linea de investigacion: {{$proyect->linea}}, elaborado bajo la tutoria del Profesor <strong>{{ $proyect['tutor']['user']->name}}</strong> cedula de identidad Nº <strong>{{ $proyect['tutor']['user']->cedula}}</strong>, Presentado por el ciudadano <strong> {{$proyect['user']->name}}, </strong> cedula de identidad Nº <strong> {{$proyect['user']->cedula}}, </strong> Atentamente  </p>

			<br> <br>
			 <table align="center">
			 	<tr><th align="center">
			 		<p class="text-center">
			 			Dra. Eddy Riera de Montero <br>
			 		Directora General de Estudios de Postgrado 
			 		</p>
			 	</th></tr>
			 </table>
		</div>	
</body>
</html>