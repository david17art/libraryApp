@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Registrar</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nombre Completo</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                       <div class="form-group{{ $errors->has('cedula') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Cedula</label>

                            <div class="col-md-6">
                                <input id="cedula" type="text" class="form-control" name="cedula" value="{{ old('cedula') }}" required autofocus>

                                @if ($errors->has('cedula'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('cedula') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Correo electronico</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Contraseña</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirmar contraseña</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="programa-especializacion" class="col-md-4 control-label">Maestria o especializacion</label>

                            <div class="col-md-6">
                                <select name="esp" class="form-control" >
                                    <option value="">Seleccionar</option>
                                    <option value="Generencia de la informacion">Generencia de la informacion</option>
                                    <option value="Generencia de la comunicacion organizacional">Generencia de la comunicacion organizacional</option>
                                    <option value="Especializacion Docencia en Educacion Superior">Especializacion Docencia en Educacion Superior</option>
                                    <option value="Especializacion en Administracion de Empresa">Especializacion en Administracion de Empresa</option>
                                    <option value="Especializacion en Administracion Industrial" >Especializacion en Administracion Industrial</option>
                                    <option value="Especializacion en Derecho Administrativo">Especializacion en Derecho Administrativo</option>
                                    <option value="Especializacion en Generencia de control de calidad y la inspeccion de obras">Especializacion en Generencia de control de calidad y la inspeccion de obras</option>
                                    <option value="Especializacion en Gestion aduanera y tributaria">Especializacion en Gestion aduanera y tributaria</option>
                                    <option value="Especializacion en Gestion y Control de las Finanzas publica">Especializacion en Gestion y Control de las Finanzas publica</option>
                                    <option value="Especializacion en Telecomunicaciones">Especializacion en Telecomunicaciones</option>
                                </select>
                            </div>
                        </div>                        

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Registrar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
