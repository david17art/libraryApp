@extends('layouts.app')

@section('content')
	<div class="container">
	    <div class="row">
	        <div class="col-md-8 col-md-offset-2">
	            <div class="panel panel-default">
	                <div class="panel-heading">Crear Proyecto</div>

	                <div class="panel-body">
	                    <form action="{{url('/app/proyecto')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
	                    	<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
	                    		<label for="" class="control-label">Titulo</label>
	                    		<input type="text" name="title" class="form-control">
								@if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif								
	                    	</div>
	                    	<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
	                    		<label for="" class="control-label">Descripcion</label>
	                    		<textarea name="description" id="" cols="30" rows="10" class="form-control"></textarea>
								@if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif								
	                    	</div>
	                    	<div class="form-group{{ $errors->has('linea') ? ' has-error' : '' }}">
	                    		<label for="" class="control-label">Linea de investigacion</label>
	                    		<input type="text" name="linea" class="form-control">
								@if ($errors->has('linea'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('linea') }}</strong>
                                    </span>
                                @endif								
	                    	</div>
	                    	<div class="form-group{{ $errors->has('grado') ? ' has-error' : '' }}">
	                    		<label for="" class="control-label">Grado de instruccion</label>
	                    		<input type="text" name="grado" class="form-control">
								@if ($errors->has('grado'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('grado') }}</strong>
                                    </span>
                                @endif								
	                    	</div>
	                    	<div class="form-group{{ $errors->has('programa') ? ' has-error' : '' }}">
	                    		<label for="" class="control-label">Programa de investigacion</label>
	                    		<input type="text" name="programa" class="form-control">
								@if ($errors->has('programa'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('programa') }}</strong>
                                    </span>
                                @endif								
	                    	</div>																					
	                    	<div class="form-group{{ $errors->has('esp') ? ' has-error' : '' }}">
	                    		<label for="" class="control-label">Especializacion</label>
	                    		<select name="esp" id="" class="form-control">
	                    			<option value="">Seleccionar</option>
									<option value="si">Si</option>
									<option value="no">No</option>
	                    		</select>
								@if ($errors->has('esp'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('esp') }}</strong>
                                    </span>
                                @endif								
	                    	</div>							
	                    	<div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
	                    		<label for="" class="control-label">Sube archivo</label>
	                    		<input type="file" accept=".pdf" name="file" class="form-control">
	                    		<small>Solo acepta archivos pdf</small>
								@if ($errors->has('file'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('file') }}</strong>
                                    </span>
                                @endif								
	                    	</div>	                    		  
							<input type="submit" value="Enviar" class="btn btn-primary">
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
@endsection