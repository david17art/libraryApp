@extends('layouts.app')

@section('content')
	<div class="container">
	    <div class="row">
	        <div class="col-md-8 col-md-offset-2">
	            <div class="panel panel-default">
	                <div class="panel-heading">Subir Trabajo</div>

	                <div class="panel-body">
	                    <form action="{{url('/app/trabajo/'.$id)}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
	                    																						
	                    	<div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
	                    		<label for="" class="control-label">Tipo</label>
	                    		<select name="type" id="" class="form-control">
	                    			<option value="">Seleccionar</option>
									<option value="TG">Trabajo de Grado</option>
									<option value="TEG">Trabajo Especial de Grado</option>
	                    		</select>
								@if ($errors->has('type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                @endif								
	                    	</div>							
	                    	<div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
	                    		<label for="" class="control-label">Sube archivo</label>
	                    		<input type="file" accept=".pdf" name="file2" class="form-control">
	                    		<small>Solo acepta archivos pdf</small>
								@if ($errors->has('file2'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('file2') }}</strong>
                                    </span>
                                @endif								
	                    	</div>	                    		  
							<input type="submit" value="Enviar" class="btn btn-primary">
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
@endsection