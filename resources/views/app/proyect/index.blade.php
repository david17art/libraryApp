@extends('layouts.app')

@section('title')
	Proyectos
@endsection

@section('content')
	<div class="container">
	    <div class="row">
	        <div class="col-md-10 col-md-offset-1">
	            <div class="row">
	            	<div class="col-md-8">
						@if($proyect->fase == 5)
							<div class="pull-right"><a href="{{url('/app/biblioteca/'.$proyecto)}}" class="btn btn-primary">Imprimir solvencia academica</a></div>			
						@endif
	        
	            		<proyect user="{{$id}}"></proyect>
	            	</div>
	            	<div class="col-md-4">
	            		<juries proyecto="{{$proyecto}}"></juries>
	            		<tutor proyecto="{{$proyecto}}"></tutor>
						<!-- Subir Trabajo de grado -->
						@if($proyect->aprobe == 1 && $proyect->fase >= 2 || $proyect->aprobe == 3 && $proyect->fase >= 2)
							@if($proyect->file2 != null)
								<div class="panel panel-success">
									<div class="panel-heading">Trabajo de Grado o Trabajo especial de Grado</div>
									<div class="panel-body">
										<a class="btn btn-block btn-primary" href="/{{$proyect->file2}}">
											Descargar
										</a>
									</div>
								</div>
							@else
								<div class="panel panel-danger">
									<div class="panel-heading">Tu proyecto fue aprobado, es hora de subir tu Trabajo de Grado o Trabajo especial de Grado</div>
									<div class="panel-body">
										<a class="btn btn-block btn-primary" href="/app/trabajo/{{$proyect->id}}">
											Subir
										</a>
									</div>
								</div>
							@endif
						@else
							<div class="panel panel-default">
								<div class="panel-heading">Pendiente por aprobacion para subir  Trabajo de Grado O Trabajo Especial de Grado</div>
							</div>
						@endif

	            	</div>
	            </div>
	        </div>
	    </div>
	</div>
@endsection