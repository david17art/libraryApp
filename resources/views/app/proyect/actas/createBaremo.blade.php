@extends('layouts.app')

@section('content')
	<div class="container">
	    <div class="row">
	        <div class="col-md-8 col-md-offset-2">
	            <div class="panel panel-default">
	                <div class="panel-heading">Crear Baremo</div>

	                <div class="panel-body">
	                    <form action="{{url('/app/baremo')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
							<div class="form-group{{ $errors->has('proyecto') ? ' has-error' : '' }}">
								<label for="" class="control-label">Proyecto</label>
								<select name="proyecto"  id="" class="form-control">
									<option value="">Seleccionar</option>
									@foreach($proyects As $proyect)
										<option value="{{$proyect->id}}">{{$proyect->title}} | Autor: {{$proyect['user']->name}}</option>
									@endforeach
								</select>
								@if ($errors->has('proyecto'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('proyecto') }}</strong>
                                    </span>
                                @endif
							</div>	
							<div class="form-group{{ $errors->has('recomendaciones') ? ' has-error' : '' }}">
								<label for="" class="control-label">Recomendaciones</label>
                                <textarea class="form-control" name="recomendaciones">
                                </textarea>
								@if ($errors->has('jurado'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('recomendaciones') }}</strong>
                                    </span>
                                @endif								
							</div>
							<!-- Generalidades -->
							<h3 class="text-center">GENERALIDADES</h3>

							<div class="row">
								<p class="text-center">a) El proyecto se ajusta a las normas vigentes del manual para la elaboracion del proyecto y trabajo final de gradode la direccion general de postgrado de la Universidad Jose Antonio Paez</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="generalidadesAPuntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="generalidadesAObservaciones" class="form-control">
									</div>
								</div>
								<p class="text-center">b) El proyecto está abscrito a una linea de investigación del Programa.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="generalidadesBPuntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="generalidadesBObservaciones" class="form-control">
									</div>
								</div>
								<p class="text-center">c) El resumen contiene: una breve introduccion, el objetivo general, la metodologia y posibles resultados o conclusiones; el texto está presentado en un solo bloque y no excede 300 palabras.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="generalidadesCPuntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="generalidadesCObservaciones" class="form-control">
									</div>
								</div>								
							</div>

							<!-- Titulo -->

							<h3 class="text-center"><strong>I TITULO</strong></h3>
							
							<div class="row">
								<p class="text-center">a) Da una visión global, concisa y clara del ambito de estudio o problema de investigacion</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="tituloAPuntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="tituloAObservaciones" class="form-control">
									</div>
								</div>
								<p class="text-center">b) Especifica el tiempo y espacio en que se realizará el estudio.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="tituloBPuntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="tituloBObservaciones" class="form-control">
									</div>
								</div>
								<p class="text-center">c) Se relaciona con el objetivo general y el contenido del trabajo.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="tituloCPuntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="tituloCObservaciones" class="form-control">
									</div>
								</div>
								<p class="text-center">d) Indica el área de conocimiento.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="tituloDPuntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="tituloDObservaciones" class="form-control">
									</div>
								</div>																
							</div>
							
							<!-- PROBLEMA O AMBITO DE ESTUDIO -->
							<h3 class="text-center"><strong>II PROBLEMA O AMBITO DE ESTUDIO</strong></h3>
							
							<div class="row">
								<p class="text-center">a) Está claramente planteado en cuanto a descripcion, causas y consecuencias. Además es congruente con el objetivo general y la linea de investigacion.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="problemaAPuntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="problemaAObservaciones" class="form-control">
									</div>
								</div>
								<p class="text-center">b) Los supuestos implícitos o las hipótesis (si es el caso) están claramente planteados. Si es el caso, reflejan las variables independientes y dependientes, y las mismas están operacionalizadas.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="problemaBPuntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="problemaBObservaciones" class="form-control">
									</div>
								</div>
								<p class="text-center">c) El problema es relevante, Aporta una alternativa de solución a nivel nacional, estadal, municipal, local a organizaciones públicas y/o privadas.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="problemaCPuntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="problemaCObservaciones" class="form-control">
									</div>
								</div>
								<p class="text-center">d) Los objetivos específicos están relacionados con el objetivo general.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="problemaDPuntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="problemaDObservaciones" class="form-control">
									</div>
								</div>	
								<p class="text-center">e) Los objetivos expresan las interrogantes a responder o aspectos desconocidos a dilucidar.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="problemaEPuntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="problemaEObservaciones" class="form-control">
									</div>
								</div>
								<p class="text-center">f) La justificacion explica el porqué y para qué se realizará el estudio, su importancia, posibles contribuciones y aportes.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="problemaFPuntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="problemaFObservaciones" class="form-control">
									</div>
								</div>																																
							</div>
							
							<!-- III. MARCO TEORICO REFERENCIAL / REVISION DE LITERATURA -->
							<h3 class="text-center"><strong>III. MARCO TEORICO REFERENCIAL / REVISION DE LITERATURA</strong></h3>
							
							<div class="row">
								<p class="text-center">a) Se relacionan los antecedentes de la investigacion con el problema planteado o ámbito de estudio.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="marcoAPuntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="marcoAObservaciones" class="form-control">
									</div>
								</div>
								<p class="text-center">b) Presenta un cuerpo teórico pertinente y actualizado con relacion al objeto/sujeto de estudio y a los objetivos de investigación, contiene un número importante de referencias a revistas y/o publicaciones periódicas del área especifica de la temática que se investiga.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="marcoBPuntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="marcoBObservaciones" class="form-control">
									</div>
								</div>
								<p class="text-center">c) Presenta un marco teórico bien organizado y pertinente.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="marcoCPuntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="marcoCObservaciones" class="form-control">
									</div>
								</div>
								<p class="text-center">d) Demuestra el conocimiento del investigador sobre los enfoques y tendencias actuales en cuanto al objeto / sujeto de estudio.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="marcoDPuntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="marcoDObservaciones" class="form-control">
									</div>
								</div>	
								<p class="text-center">e) Estan definidos operacionales los terminos innovadores, relevantes y excepciones; Si es pertinente.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="marcoEPuntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="marcoEObservaciones" class="form-control">
									</div>
								</div>
								<p class="text-center">f) La bibliografia está actualizada; es decir, no ha caducado ni tiene mas de cinco(05) años.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="marcoFPuntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="marcoFObservaciones" class="form-control">
									</div>
								</div>																																
							</div>							
							<!-- IV. METODOLOGIA -->
							<h3 class="text-center"><strong>IV. METODOLOGIA</strong></h3>
							
							<div class="row">
								<p class="text-center">a) Se describen claramente el tipo, los metodos y nivel de investigacion.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="metodologiaAPuntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="metodologiaAObservaciones" class="form-control">
									</div>
								</div>
								<p class="text-center">b) Se describe claramente el diseño de investigacion y las tecnicas e instrumentos de recoleccion de datos.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="metodologiaBPuntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="metodologiaBObservaciones" class="form-control">
									</div>
								</div>
								<p class="text-center">c) El diseño de la investigacion es apropiado para abordar la alternativa de solucion propuesta y describe los procedimientos requeridos.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="metodologiaCPuntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="metodologiaCObservaciones" class="form-control">
									</div>
								</div>
								<p class="text-center">d) Si es el caso, la operacionalizacion de las variables conduce a la elaboracion de instrumento que se va a aplicar.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="metodologiaDPuntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="metodologiaDObservaciones" class="form-control">
									</div>
								</div>	
								<p class="text-center">e) Las tecnicas o preocedimientos para analizar la informacion son apropiados para el tipo de investigacion que se va a realizar.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="metodologiaEPuntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="metodologiaEObservaciones" class="form-control">
									</div>
								</div>
								<p class="text-center">f) Se describe la poblacion y muestra) informantes claves segun el enfoque de investigacion seleccionado.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="metodologiaFPuntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="metodologiaFObservaciones" class="form-control">
									</div>
								</div>																																
							</div>
							
							
							<!-- V. ASPECTOS ADMINISTRATIVOS -->
							<h3 class="text-center"><strong>V. ASPECTOS ADMINISTRATIVOS</strong></h3>
							
							<div class="row">
								<p class="text-center">a) Dispone el investigador de la formacion teorica y cientifica para abordar el problema; y ademas dispone de la infraestructura tecnologica necesaria para desarrollar el proyecto.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="aspectosAPuntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="aspectosAObservaciones" class="form-control">
									</div>
								</div>
								<p class="text-center">b) Evidencia disponer de la infraestructura y logistica adecuada para desarrollar el proyecto.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="aspectosBPuntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="aspectosBObservaciones" class="form-control">
									</div>
								</div>
								<p class="text-center">c) Presenta el investigador los recursos financieros bien sean propios o de subvencion, necesarios para cubrir los costos del proyecto.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="aspectosCPuntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="aspectosCObservaciones" class="form-control">
									</div>
								</div>
								<p class="text-center">d) Relaciona el personal necesario para realizar la investigacion.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="aspectosDPuntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="aspectosDObservaciones" class="form-control">
									</div>
								</div>	
								<p class="text-center">e) Presenta un cronograma de actividades organizado por tareas indicando los lapsos de ejecución.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="aspectosEPuntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="aspectosEObservaciones" class="form-control">
									</div>
								</div>																															
							</div>
							
														<!-- VI. FORMA Y ESTILO -->
							<h3 class="text-center"><strong>VI. FORMA Y ESTILO</strong></h3>
							
							<div class="row">
								<p class="text-center">a) Uso del lenguaje cientifico: precision, uso de la terminologia pertinente.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="formaAPuntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="formaAObservaciones" class="form-control">
									</div>
								</div>
								<p class="text-center">b) Uso de lenguaje materno.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="formaBPuntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="formaBObservaciones" class="form-control">
									</div>
								</div>
								<p class="text-center">b.1 El proyecto está claramente redactado.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="formaB1Puntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="formaB1Observaciones" class="form-control">
									</div>
								</div>
								<p class="text-center">b.2 Utiliza una gramatica y lenguaje apropiados.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="formaB2Puntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="formaB2Observaciones" class="form-control">
									</div>
								</div>	
								<p class="text-center">b.3 Revela una buena ortografia.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="formaB3Puntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="formaB3Observaciones" class="form-control">
									</div>
								</div>
								<p class="text-center">c) Uso correcto de las Normas APA.</p>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Puntaje</label>
										<input type="number" name="formaCPuntaje" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="" class="control-label">Observaciones</label>
										<input type="text" name="formaCObservaciones" class="form-control">
									</div>
								</div>																																
							</div>

							<input type="submit" value="Generar" class="btn btn-primary">
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
@endsection