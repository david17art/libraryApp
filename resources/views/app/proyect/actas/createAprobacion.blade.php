@extends('layouts.app')

@section('content')
	<div class="container">
	    <div class="row">
	        <div class="col-md-8 col-md-offset-2">
	            <div class="panel panel-default">
	                <div class="panel-heading">Crear Acta de Aprobacion de proyecto y designacion de tutora</div>

	                <div class="panel-body">
	                    <form action="{{url('/app/aprobacion')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
							<div class="form-group{{ $errors->has('proyecto') ? ' has-error' : '' }}">
								<label for="" class="control-label">Proyecto</label>
								<select name="proyecto"  id="" class="form-control">
									<option value="">Seleccionar</option>
									@foreach($proyects As $proyect)
										<option value="{{$proyect->id}}">{{$proyect->title}} | Autor: {{$proyect['user']->name}}</option>
									@endforeach
								</select>
								@if ($errors->has('proyecto'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('proyecto') }}</strong>
                                    </span>
                                @endif
							</div>															
							<input type="submit" value="Generar" class="btn btn-primary">
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
@endsection