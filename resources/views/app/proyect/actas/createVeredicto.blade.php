@extends('layouts.app')

@section('content')
	<div class="container">
	    <div class="row">
	        <div class="col-md-8 col-md-offset-2">
	            <div class="panel panel-default">
	                <div class="panel-heading">Crear Veredicto</div>

	                <div class="panel-body">
	                    <form action="{{url('/app/veredicto')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
							<div class="form-group{{ $errors->has('proyecto') ? ' has-error' : '' }}">
								<label for="" class="control-label">Proyecto</label>
								<select name="proyecto"  id="" class="form-control">
									<option value="">Seleccionar</option>
									@foreach($proyects As $proyect)
										<option value="{{$proyect->id}}">{{$proyect->title}} | Autor: {{$proyect['user']->name}}</option>
									@endforeach
								</select>
								@if ($errors->has('proyecto'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('proyecto') }}</strong>
                                    </span>
                                @endif
							</div>
							<div class="form-group{{ $errors->has('presidente') ? ' has-error' : '' }}">
								<label for="" class="control-label">Presidente</label>
								<input type="text" name="presidente" class="form-control">
								@if ($errors->has('presidente'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('presidente') }}</strong>
                                    </span>
                                @endif								
							</div>
							<div class="form-group{{ $errors->has('cedulaP') ? ' has-error' : '' }}">
								<label for="" class="control-label">Cedula de Presidente</label>
								<input type="text" name="cedulaP" class="form-control">
								@if ($errors->has('cedulaP'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('cedulaP') }}</strong>
                                    </span>
                                @endif								
							</div>	
							<div class="form-group{{ $errors->has('miembro1') ? ' has-error' : '' }}">
								<label for="" class="control-label">Miembro 1</label>
								<input type="text" name="miembro1" class="form-control">
								@if ($errors->has('miembro1'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('miembro1') }}</strong>
                                    </span>
                                @endif								
							</div>
							<div class="form-group{{ $errors->has('cedulaM1') ? ' has-error' : '' }}">
								<label for="" class="control-label">Cedula de Miembro 1</label>
								<input type="text" name="cedulaM1" class="form-control">
								@if ($errors->has('cedulaM1'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('cedulaM1') }}</strong>
                                    </span>
                                @endif								
							</div>	
							<div class="form-group{{ $errors->has('miembro2') ? ' has-error' : '' }}">
								<label for="" class="control-label">Miembro 2</label>
								<input type="text" name="miembro2" class="form-control">
								@if ($errors->has('miembro2'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('miembro2') }}</strong>
                                    </span>
                                @endif								
							</div>
							<div class="form-group{{ $errors->has('cedulaM2') ? ' has-error' : '' }}">
								<label for="" class="control-label">Cedula de Miembro 2</label>
								<input type="text" name="cedulaM2" class="form-control">
								@if ($errors->has('cedulaM2'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('cedulaM2') }}</strong>
                                    </span>
                                @endif								
							</div>								
							<input type="submit" value="Generar" class="btn btn-primary">
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
@endsection