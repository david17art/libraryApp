@extends('layouts.app')


@section('content')
	<div class="container">
	    <div class="row">
	        <div class="col-md-8 col-md-offset-2">
	            <div class="panel panel-default">
	                <div class="panel-heading">Actas</div>

	                <div class="panel-body">
	                   <div class="row">
					   		<ul class="list">
							   <h3>Fase 1</h3>
							   	<li class="list-item">
							   		<a href="{{url('/app/baremo')}}">Baremo</a>
								</li>
								<li class="list-item">
									<a href="{{url('/app/aprobacion')}}">Acta de Aprobación de Proyecto y designación de Jurado</a>
								</li>  
								<h3>Fase 2</h3>
								<li class="list-item">
									<a href="{{url('/app/invitacion')}}">Acta de Invitación al Jurado</a>
								</li> 
								<li class="list-item">
									<a href="{{url('/app/actaJurado')}}">Acta de Constitución de Jurado</a>
								</li>
								<h3>Fase 3</h3>
								<li class="list-item">
									<a href="{{url('/app/veredicto')}}">Veredicto</a>
								</li>

							</ul>	 	                   		                  		
	                   </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
@endsection