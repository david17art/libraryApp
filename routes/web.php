<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.welcome');
});


Route::get('registrarProfesor','ProfessorController@registerForm');
Route::post('registerProfessor','ProfessorController@register');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'],function(){
	Route::group(['prefix' => 'app'],function(){
		Route::resource('proyecto','ProyectController');
		Route::get('getProyects','ProyectController@getProyects');
		Route::get('getProyect/{user}','ProyectController@getProyect');
		Route::get('getProyectsAll/{action}','ProyectController@getProyectsAll');
		Route::put('aprobe/{proyect}','ProyectController@aprobe');
		Route::get('getMembers/{proyecto}','ProyectController@getMembers');
		Route::post('member','ProyectController@member');
		Route::get('getTutors/{proyecto}','ProyectController@getTutors');
		Route::post('tutor','ProyectController@tutor');
		Route::get('getUsers','ProyectController@getUsers');
		Route::get('getUsersProfessor','ProyectController@getUsersProfessor');
		Route::get('getJuries/{proyecto}','ProyectController@getJuries');
		Route::post('jury/{proyecto}','ProyectController@jury');

		Route::get('trabajo/{id}','ProyectController@getFormTrabajo');
		Route::post('trabajo/{id}','ProyectController@trabajo');

		//Pdf
		Route::get('actas','ProyectController@indexActas');
		Route::get('actaJurado','ProyectController@formActaDeJurado');
		Route::post('actaJurado','ProyectController@ActaDeJurado');

		Route::post('veredicto','ProyectController@veredicto');
		Route::get('veredicto','ProyectController@formVeredicto');

		Route::get('aprobacion','ProyectController@formAprobacion');
		Route::post('aprobacion','ProyectController@aprobacion');
		Route::get('test/{proyecto}','ProyectController@test');

		Route::get('invitacion','ProyectController@formInvitacion');
		Route::post('invitacion','ProyectController@invitacion');
		Route::get('biblioteca/{id}','ProyectController@biblioteca');

		Route::get('baremo','ProyectController@formBaremo');
		Route::post('baremo','ProyectController@baremo');
	});
	
});
